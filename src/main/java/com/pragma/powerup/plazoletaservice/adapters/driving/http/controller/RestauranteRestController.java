package com.pragma.powerup.plazoletaservice.adapters.driving.http.controller;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IRestauranteHandler;
import com.pragma.powerup.plazoletaservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/restaurante")
@RequiredArgsConstructor
//@SecurityRequirement(name = "jwt")
public class RestauranteRestController {
    private final IRestauranteHandler restauranteHandler;

    @Operation(summary = "Add a new Restaurante",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Restaurante created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Restaurante already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Restaurante not allowed for owner creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("")
    //@PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Map<String, String>> saveRestaurante(@Valid @RequestBody RestauranteRequestDto restauranteRequestDto) {
        restauranteHandler.saveRestaurante(restauranteRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.RESTAURANTE_CREATED_MESSAGE));
    }
}
