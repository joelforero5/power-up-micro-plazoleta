package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class RoleNotFoundException extends RuntimeException {
    public RoleNotFoundException() {
        super();
    }
}
