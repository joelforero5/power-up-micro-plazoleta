package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.RestauranteRequestDto;

public interface IRestauranteHandler {
    void saveRestaurante(RestauranteRequestDto restauranteRequestDto);
}
