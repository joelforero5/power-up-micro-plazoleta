package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.impl;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IRestauranteHandler;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IRestauranteRequestMapper;
import com.pragma.powerup.plazoletaservice.domain.api.IRestauranteServicePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RestauranteHandlerImpl implements IRestauranteHandler {
    private final IRestauranteServicePort restauranteServicePort;
    private final IRestauranteRequestMapper restauranteRequestMapper;
    @Override
    public void saveRestaurante(RestauranteRequestDto restauranteRequestDto) {
        restauranteServicePort.saveRestaurante(restauranteRequestMapper.toRestaurante(restauranteRequestDto));
    }
}
