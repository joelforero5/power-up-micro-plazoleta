package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RestauranteRequestDto {
    @NotBlank
    @Pattern(regexp = "^(?=.*[a-zA-Z])\\w+$", message = "El nombre del restaurante no es válido, no se permiten nombres con solo numeros")
    private String nombre;
    @NotBlank
    private String direccion;
    @NotNull
    private Long idPropietario;
    @NotBlank
    @Pattern(regexp = "^\\+?\\d{1,12}$", message = "El número de celular debe tener un formato valido")
    private String telefono;
    @NotBlank
    private String urlLogo;
    @NotBlank
    @Pattern(regexp = "^[0-9]+$", message = "El numero de documento debe ser numérico")
    private String nit;
}
