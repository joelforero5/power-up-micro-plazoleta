package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IRestauranteEntityMapper;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IRestauranteRepository;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional
public class RestauranteMysqlAdapter implements IRestaurantePersistencePort {
    private final IRestauranteRepository restauranteRepository;
    private final IRestauranteEntityMapper restauranteEntityMapper;


    @Override
    public void saveRestaurante(Restaurante restaurante) {
        restauranteRepository.save(restauranteEntityMapper.toEntity(restaurante));
    }
}
