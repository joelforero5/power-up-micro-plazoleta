package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class UserAlreadyExistsException extends RuntimeException {
    public UserAlreadyExistsException() {
        super();
    }
}
