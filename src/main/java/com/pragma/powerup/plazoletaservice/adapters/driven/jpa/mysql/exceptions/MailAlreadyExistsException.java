package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class MailAlreadyExistsException extends RuntimeException {
    public MailAlreadyExistsException() {
        super();
    }
}
