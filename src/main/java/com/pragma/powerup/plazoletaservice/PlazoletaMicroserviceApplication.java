package com.pragma.powerup.plazoletaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlazoletaMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlazoletaMicroserviceApplication.class, args);
	}

}
