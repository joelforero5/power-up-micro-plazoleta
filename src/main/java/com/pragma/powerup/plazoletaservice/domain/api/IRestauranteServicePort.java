package com.pragma.powerup.plazoletaservice.domain.api;

import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;

public interface IRestauranteServicePort {
    void saveRestaurante(Restaurante restaurante);
}
