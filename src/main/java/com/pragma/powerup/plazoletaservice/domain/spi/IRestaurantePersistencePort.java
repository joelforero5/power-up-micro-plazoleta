package com.pragma.powerup.plazoletaservice.domain.spi;

import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;

public interface IRestaurantePersistencePort {

    void saveRestaurante(Restaurante restaurante);
}
