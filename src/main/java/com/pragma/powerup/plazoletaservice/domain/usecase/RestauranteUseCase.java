package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.domain.api.IRestauranteServicePort;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;

public class RestauranteUseCase implements IRestauranteServicePort {

    private final IRestaurantePersistencePort restaurantePersistencePort;

    public RestauranteUseCase(IRestaurantePersistencePort restaurantePersistencePort) {
        this.restaurantePersistencePort = restaurantePersistencePort;
    }

    @Override
    public void saveRestaurante(Restaurante restaurante) {
        restaurantePersistencePort.saveRestaurante(restaurante);
    }
}
