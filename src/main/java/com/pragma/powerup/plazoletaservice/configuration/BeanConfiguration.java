package com.pragma.powerup.plazoletaservice.configuration;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter.RestauranteMysqlAdapter;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IRestauranteEntityMapper;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IRestauranteRepository;
import com.pragma.powerup.plazoletaservice.domain.api.IRestauranteServicePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import com.pragma.powerup.plazoletaservice.domain.usecase.RestauranteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final IRestauranteRepository restauranteRepository;
    private final IRestauranteEntityMapper restauranteEntityMapper;

    @Bean
    public IRestaurantePersistencePort restaurantePersistencePort() {
        return new RestauranteMysqlAdapter(restauranteRepository, restauranteEntityMapper);
    }
    @Bean
    public IRestauranteServicePort restauranteServicePort() {
        return new RestauranteUseCase(restaurantePersistencePort());
    }


}
